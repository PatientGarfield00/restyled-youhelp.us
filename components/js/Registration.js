let patient;
let volunteer;
let employee;

window.onload = () => {
  patient = document.getElementById("patient");
  volunteer = document.getElementById("volunteer");
  employee = document.getElementById("employee");
};

const proceedRegistration = () => {
  if (patient.checked) {
    console.log(patient.id);
    window.location.href = "../html/RegistrationPatient.html";
  }
  if (volunteer.checked) {
    console.log(volunteer.id);
    window.location.href = "../html/RegistrationVolunteer.html";
  }
  if (employee.checked) {
    console.log(employee.id);
    window.location.href = "../html/RegistrationEmployee.html";
  }
};
