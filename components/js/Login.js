let errorObject;

window.onload = () => {
  errorObject = document.getElementById("error");
  errorObject.style.visibility = "hidden";
};

const login = () => {
  let username = document.getElementById("username").value;
  let pass = document.getElementById("password").value;

  if (username !== "" && pass !== "") {
    errorObject.style.visibility = "hidden";
    console.log("Your username is: ", username);
    console.log("Your password is: ", pass);
  } else {
    errorObject.style.visibility = "visible";
  }
};
